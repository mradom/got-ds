<?php
/* @var $this TecnicoOrdenController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Tecnico Ordens',
);

$this->menu=array(
	array('label'=>'Crear TecnicoOrden', 'url'=>array('create')),
	array('label'=>'Administrar TecnicoOrden', 'url'=>array('admin')),
);
?>

<h1>Tecnico Ordens</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
