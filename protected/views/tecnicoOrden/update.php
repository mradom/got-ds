<?php
/* @var $this TecnicoOrdenController */
/* @var $model TecnicoOrden */

$this->breadcrumbs=array(
	'Tecnico Ordens'=>array('index'),
	$model->toid=>array('view','id'=>$model->toid),
	'Actualizar',
);

$this->menu=array(
	array('label'=>'Listar TecnicoOrden', 'url'=>array('index')),
	array('label'=>'Crear TecnicoOrden', 'url'=>array('create')),
	array('label'=>'Ver TecnicoOrden', 'url'=>array('view', 'id'=>$model->toid)),
	array('label'=>'Administrar TecnicoOrden', 'url'=>array('admin')),
);
?>

<h1>Actualizar TecnicoOrden <?php echo $model->toid; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>