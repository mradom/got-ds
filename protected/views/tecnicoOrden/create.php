<?php
/* @var $this TecnicoOrdenController */
/* @var $model TecnicoOrden */

$this->breadcrumbs=array(
	'Tecnico Ordens'=>array('index'),
	'Crear',
);

$this->menu=array(
	array('label'=>'Listar TecnicoOrden', 'url'=>array('index')),
	array('label'=>'Admistrar TecnicoOrden', 'url'=>array('admin')),
);
?>

<h1>Crear TecnicoOrden</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>