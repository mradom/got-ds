<?php
/* @var $this TecnicoOrdenController */
/* @var $data TecnicoOrden */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('toid')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->toid), array('view', 'id'=>$data->toid)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tid')); ?>:</b>
	<?php echo CHtml::encode($data->tid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('oid')); ?>:</b>
	<?php echo CHtml::encode($data->oid); ?>
	<br />


</div>