<?php
/* @var $this TecnicoPorcentajeController */
/* @var $model TecnicoPorcentaje */

$this->breadcrumbs=array(
	'Tecnico Porcentajes'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Listar TecnicoPorcentaje', 'url'=>array('index')),
	array('label'=>'Crear TecnicoPorcentaje', 'url'=>array('create')),
	array('label'=>'Actulizar TecnicoPorcentaje', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Borrar TecnicoPorcentaje', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Administrar TecnicoPorcentaje', 'url'=>array('admin')),
);
?>

<h1>Ver Tecnico Porcentaje #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'tecnico0.username',
		'aparato0.aparato',
		'porcentaje',
	),
)); ?>
