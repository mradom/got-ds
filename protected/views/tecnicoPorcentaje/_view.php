<?php
/* @var $this TecnicoPorcentajeController */
/* @var $data TecnicoPorcentaje */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tecnico')); ?>:</b>
	<?php echo CHtml::encode($data->tecnico); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('aparato')); ?>:</b>
	<?php echo CHtml::encode($data->aparato); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('porcentaje')); ?>:</b>
	<?php echo CHtml::encode($data->porcentaje); ?>
	<br />


</div>