<?php
/* @var $this TecnicoPorcentajeController */
/* @var $model TecnicoPorcentaje */

$this->breadcrumbs=array(
	'Tecnico Porcentajes'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Actualizar',
);

$this->menu=array(
	array('label'=>'Listar TecnicoPorcentaje', 'url'=>array('index')),
	array('label'=>'Crear TecnicoPorcentaje', 'url'=>array('create')),
	array('label'=>'Ver TecnicoPorcentaje', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Administrar TecnicoPorcentaje', 'url'=>array('admin')),
);
?>

<h1>Actualizar TecnicoPorcentaje <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>