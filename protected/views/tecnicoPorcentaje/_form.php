<?php
/* @var $this TecnicoPorcentajeController */
/* @var $model TecnicoPorcentaje */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'tecnico-porcentaje-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Campos con <span class="required">*</span> son requeridos.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'tecnico'); ?>
		<?php echo $form->textField($model->tecnico0,'username', array('readonly' => true,)); ?>
		<?php echo $form->error($model,'tecnico'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'aparato'); ?>
		<?php echo $form->textField($model->aparato0,'aparato', array('readonly' => true,)); ?>
		<?php echo $form->error($model,'aparato'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'porcentaje'); ?>
		<?php echo $form->textField($model,'porcentaje'); ?>
		<?php echo $form->error($model,'porcentaje'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Crear' : 'Guardar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->