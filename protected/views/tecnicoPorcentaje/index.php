<?php
/* @var $this TecnicoPorcentajeController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Tecnico Porcentajes',
);

$this->menu=array(
	array('label'=>'Crear TecnicoPorcentaje', 'url'=>array('create')),
	array('label'=>'Administrar TecnicoPorcentaje', 'url'=>array('admin')),
);
?>

<h1>Tecnico Porcentajes</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
