<?php
/* @var $this TecnicoPorcentajeController */
/* @var $model TecnicoPorcentaje */

$this->breadcrumbs=array(
	'Tecnico Porcentajes'=>array('index'),
	'Crear',
);

$this->menu=array(
	array('label'=>'Listar TecnicoPorcentaje', 'url'=>array('index')),
	array('label'=>'Admistrar TecnicoPorcentaje', 'url'=>array('admin')),
);
?>

<h1>Crear TecnicoPorcentaje</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>