<?php
/* @var $this GetReporteTecnicoController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Get Reporte Tecnicos',
);

$this->menu=array(
	array('label'=>'Crear GetReporteTecnico', 'url'=>array('create')),
	array('label'=>'Administrar GetReporteTecnico', 'url'=>array('admin')),
);
?>

<h1>Get Reporte Tecnicos</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
