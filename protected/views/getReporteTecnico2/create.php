<?php
/* @var $this GetReporteTecnicoController */
/* @var $model GetReporteTecnico */

$this->breadcrumbs=array(
	'Get Reporte Tecnicos'=>array('index'),
	'Crear',
);

$this->menu=array(
	array('label'=>'Listar GetReporteTecnico', 'url'=>array('index')),
	array('label'=>'Admistrar GetReporteTecnico', 'url'=>array('admin')),
);
?>

<h1>Crear GetReporteTecnico</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>