<?php
/* @var $this GetReporteTecnicoController */
/* @var $model GetReporteTecnico */

$this->breadcrumbs=array(
	'Reporte Tecnicos'=>array('index'),
	'Administrar',
);

$this->menu=array(
	//array('label'=>'Listar GetReporteTecnico', 'url'=>array('index')),
	//array('label'=>'Crear ReporteTecnico', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
");

/**
$('.search-form form').submit(function(){
	$('#get-reporte-tecnico-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
*/
?>

<h1>Administrar Reporte Tecnicos</h1>

<!--<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>-->

<div class="search-form">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'get-reporte-tecnico-grid',
	'dataProvider'=>$model->search(),
	'columns'=>array(
		'orden_id',
		'estado',
		'username',
		//'iduser',
		'fecha',
		//'id',
		'totalRep',
		'importe',
		'porcentaje',
		array(
			'name'=>'aPagar',
			'type'=>'text',
			'footer'=>$model->getTotals($model->search()->getData()),
		),
	),
)); ?>