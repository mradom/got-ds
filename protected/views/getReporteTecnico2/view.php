<?php
/* @var $this GetReporteTecnicoController */
/* @var $model GetReporteTecnico */

$this->breadcrumbs=array(
	'Get Reporte Tecnicos'=>array('index'),
	$model->orden_id,
);

$this->menu=array(
	array('label'=>'Listar GetReporteTecnico', 'url'=>array('index')),
	array('label'=>'Crear GetReporteTecnico', 'url'=>array('create')),
	array('label'=>'Actulizar GetReporteTecnico', 'url'=>array('update', 'id'=>$model->orden_id)),
	array('label'=>'Borrar GetReporteTecnico', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->orden_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Administrar GetReporteTecnico', 'url'=>array('admin')),
);
?>

<h1>Ver GetReporteTecnico #<?php echo $model->orden_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'orden_id',
		'estado',
		'username',
		'iduser',
		'fecha',
		'id',
		'totalRep',
		'importe',
	),
)); ?>
