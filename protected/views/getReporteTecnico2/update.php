<?php
/* @var $this GetReporteTecnicoController */
/* @var $model GetReporteTecnico */

$this->breadcrumbs=array(
	'Get Reporte Tecnicos'=>array('index'),
	$model->orden_id=>array('view','id'=>$model->orden_id),
	'Actualizar',
);

$this->menu=array(
	array('label'=>'Listar GetReporteTecnico', 'url'=>array('index')),
	array('label'=>'Crear GetReporteTecnico', 'url'=>array('create')),
	array('label'=>'Ver GetReporteTecnico', 'url'=>array('view', 'id'=>$model->orden_id)),
	array('label'=>'Administrar GetReporteTecnico', 'url'=>array('admin')),
);
?>

<h1>Actualizar GetReporteTecnico <?php echo $model->orden_id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>