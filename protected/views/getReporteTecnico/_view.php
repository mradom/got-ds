<?php
/* @var $this GetReporteTecnicoController */
/* @var $data GetReporteTecnico */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('orden_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->orden_id), array('view', 'id'=>$data->orden_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::encode($data->id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('aparato')); ?>:</b>
	<?php echo CHtml::encode($data->aparato); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fecha_terminada')); ?>:</b>
	<?php echo CHtml::encode($data->fecha_terminada); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('username')); ?>:</b>
	<?php echo CHtml::encode($data->username); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('iduser')); ?>:</b>
	<?php echo CHtml::encode($data->iduser); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('importe')); ?>:</b>
	<?php echo CHtml::encode($data->importe); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('porcentaje')); ?>:</b>
	<?php echo CHtml::encode($data->porcentaje); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('aPagar')); ?>:</b>
	<?php echo CHtml::encode($data->aPagar); ?>
	<br />

	*/ ?>

</div>