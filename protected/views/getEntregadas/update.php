<?php
/* @var $this GetEntregadasController */
/* @var $model GetEntregadas */

$this->breadcrumbs=array(
	'Get Entregadases'=>array('index'),
	$model->orden_id=>array('view','id'=>$model->orden_id),
	'Actualizar',
);

$this->menu=array(
	array('label'=>'Listar GetEntregadas', 'url'=>array('index')),
	array('label'=>'Crear GetEntregadas', 'url'=>array('create')),
	array('label'=>'Ver GetEntregadas', 'url'=>array('view', 'id'=>$model->orden_id)),
	array('label'=>'Administrar GetEntregadas', 'url'=>array('admin')),
);
?>

<h1>Actualizar GetEntregadas <?php echo $model->orden_id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>