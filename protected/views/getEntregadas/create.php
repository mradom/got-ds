<?php
/* @var $this GetEntregadasController */
/* @var $model GetEntregadas */

$this->breadcrumbs=array(
	'Get Entregadases'=>array('index'),
	'Crear',
);

$this->menu=array(
	array('label'=>'Listar GetEntregadas', 'url'=>array('index')),
	array('label'=>'Admistrar GetEntregadas', 'url'=>array('admin')),
);
?>

<h1>Crear GetEntregadas</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>