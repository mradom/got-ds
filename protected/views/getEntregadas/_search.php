<?php
/* @var $this GetEntregadasController */
/* @var $model GetEntregadas */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'fecha_desde'); ?>
		       <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
               'model' => $model,
               'attribute' => 'fecha_desde',
               'language' => 'es',
               'htmlOptions' => array(
                       'size' => '10',         // textField size
                       'maxlength' => '10',    // textField maxlength
               ),
               'options' => array(
                       'showOn' => 'both',             // also opens with a button
                       'dateFormat' => 'yy-mm-dd',     // format of "2012-12-25"
                       'showOtherMonths' => true,      // show dates in other months
                       'selectOtherMonths' => true,    // can seelect dates in other months
                       'changeYear' => true,           // can change year
                       'changeMonth' => true,          // can change month
                       'yearRange' => '2016:2099',     // range of year
                       'showButtonPanel' => true,      // show button panel
               ),
       )); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'fecha_hasta'); ?>
		       <?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
               'model' => $model,
               'attribute' => 'fecha_hasta',
               'language' => 'es',
               'htmlOptions' => array(
                       'size' => '10',         // textField size
                       'maxlength' => '10',    // textField maxlength
               ),
               'options' => array(
                       'showOn' => 'both',             // also opens with a button
                       'dateFormat' => 'yy-mm-dd',     // format of "2012-12-25"
                       'showOtherMonths' => true,      // show dates in other months
                       'selectOtherMonths' => true,    // can seelect dates in other months
                       'changeYear' => true,           // can change year
                       'changeMonth' => true,          // can change month
                       'yearRange' => '2016:2099',     // range of year
                       'showButtonPanel' => true,      // show button panel
               ),
       )); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Buscar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->