<?php
/* @var $this GetEntregadasController */
/* @var $model GetEntregadas */

$this->breadcrumbs=array(
	'Get Entregadases'=>array('index'),
	$model->orden_id,
);

$this->menu=array(
	array('label'=>'Listar GetEntregadas', 'url'=>array('index')),
	array('label'=>'Crear GetEntregadas', 'url'=>array('create')),
	array('label'=>'Actulizar GetEntregadas', 'url'=>array('update', 'id'=>$model->orden_id)),
	array('label'=>'Borrar GetEntregadas', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->orden_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Administrar GetEntregadas', 'url'=>array('admin')),
);
?>

<h1>Ver GetEntregadas #<?php echo $model->orden_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'orden_id',
		'estado_id',
		'fecha',
		'aparato',
		'marca',
		'modelo',
		'importe',
	),
)); ?>
