<?php
/* @var $this GetEntregadasController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Get Entregadases',
);

$this->menu=array(
	array('label'=>'Crear GetEntregadas', 'url'=>array('create')),
	array('label'=>'Administrar GetEntregadas', 'url'=>array('admin')),
);
?>

<h1>Get Entregadases</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
