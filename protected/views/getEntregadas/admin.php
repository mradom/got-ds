<?php
/* @var $this GetEntregadasController */
/* @var $model GetEntregadas */

$this->breadcrumbs=array(
	'Reportes Entregados'=>array('index'),
	'Administrar',
);

$this->menu=array();

Yii::app()->clientScript->registerScript('search', "
	$('.search-form').toggle();
");

/**
$('.search-form form').submit(function(){
	$('#get-reporte-tecnico-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
*/
?>

<h1>Administrar Get Entregadases</h1>

<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'get-entregadas-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'orden_id',
		'fecha',
		'aparato',
		'marca',
		'modelo',
		'importe',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
