<?php
/* @var $this RepuestosAlternativosController */
/* @var $model RepuestosAlternativos */

$this->breadcrumbs=array(
	'Repuestos Alternativoses'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Listar RepuestosAlternativos', 'url'=>array('index')),
	array('label'=>'Crear RepuestosAlternativos', 'url'=>array('create')),
	array('label'=>'Actulizar RepuestosAlternativos', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Borrar RepuestosAlternativos', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Administrar RepuestosAlternativos', 'url'=>array('admin')),
);
?>

<h1>Ver RepuestosAlternativos #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'marca',
		'modelo',
		'codigo',
		'detalle',
		'precio',
		'cantidad',
	),
)); ?>
