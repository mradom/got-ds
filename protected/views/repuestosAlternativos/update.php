<?php
/* @var $this RepuestosAlternativosController */
/* @var $model RepuestosAlternativos */

$this->breadcrumbs=array(
	'Repuestos Alternativoses'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Actualizar',
);

$this->menu=array(
	array('label'=>'Listar RepuestosAlternativos', 'url'=>array('index')),
	array('label'=>'Crear RepuestosAlternativos', 'url'=>array('create')),
	array('label'=>'Ver RepuestosAlternativos', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Administrar RepuestosAlternativos', 'url'=>array('admin')),
);
?>

<h1>Actualizar RepuestosAlternativos <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>