<?php
/* @var $this RepuestosAlternativosController */
/* @var $model RepuestosAlternativos */

$this->breadcrumbs=array(
	'Repuestos Alternativoses'=>array('index'),
	'Crear',
);

$this->menu=array(
	array('label'=>'Listar RepuestosAlternativos', 'url'=>array('index')),
	array('label'=>'Admistrar RepuestosAlternativos', 'url'=>array('admin')),
);
?>

<h1>Crear RepuestosAlternativos</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>