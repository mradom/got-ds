<?php
/* @var $this RepuestosAlternativosController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Repuestos Alternativoses',
);

$this->menu=array(
	array('label'=>'Crear RepuestosAlternativos', 'url'=>array('create')),
	array('label'=>'Administrar RepuestosAlternativos', 'url'=>array('admin')),
);
?>

<h1>Repuestos Alternativoses</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
