<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'get-ordenes-aprobadas-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'aparato',
		'marca',
		'descripcion',
		'fecha',
		'importe',
		array(
			'class'=>'CButtonColumn',
			'header'=>'Acciones',
			'template'=>'{view}',
		),
	),
)); ?>