<?php

/**
 * This is the model class for table "getEntregadas".
 *
 * The followings are the available columns in table 'getEntregadas':
 * @property integer $orden_id
 * @property integer $estado_id
 * @property string $fecha
 * @property string $aparato
 * @property string $marca
 * @property string $modelo
 * @property double $importe
 */
class GetEntregadas extends CActiveRecord
{

    public $fecha_desde;
    public $fecha_hasta;

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return GetEntregadas the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'getEntregadas';
	}

    public function primaryKey(){
        return 'orden_id';
    }


	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('orden_id, estado_id, aparato, marca, modelo', 'required'),
			array('orden_id, estado_id', 'numerical', 'integerOnly'=>true),
			array('importe', 'numerical'),
			array('aparato, marca', 'length', 'max'=>50),
			array('modelo', 'length', 'max'=>255),
			array('fecha', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('orden_id, estado_id, fecha, aparato, marca, modelo, importe', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

    public function getTotals($ids)
    {
        if (!empty($ids)) {
            $ids = implode(",",$ids);
            $connection=Yii::app()->db;
            $command=$connection->createCommand("SELECT SUM(importe) FROM `orden_importe` where oid in ($ids)");
            return "Total ARS: ".$amount = $command->queryScalar();
        }else{
            return "Total ARS: 0";
        }
    }

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'orden_id' => 'Orden',
			'estado_id' => 'Estado',
			'fecha' => 'Fecha',
			'aparato' => 'Aparato',
			'marca' => 'Marca',
			'modelo' => 'Modelo',
			'importe' => 'Importe',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{

		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('orden_id',$this->orden_id);
		$criteria->compare('estado_id',$this->estado_id);
		$criteria->compare('fecha',$this->fecha,true);
		$criteria->compare('aparato',$this->aparato,true);
		$criteria->compare('marca',$this->marca,true);
		$criteria->compare('modelo',$this->modelo,true);
		$criteria->compare('importe',$this->importe);

        if(($this->fecha_desde != '') && ($this->fecha_hasta != '')) {
            $criteria->condition = "fecha BETWEEN '{$this->fecha_desde}' AND '{$this->fecha_hasta}'";
        }

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'sort' => array('defaultOrder' => 'fecha desc'),
            'pagination'=>array('pageSize'=>50),
        ));
	}
}