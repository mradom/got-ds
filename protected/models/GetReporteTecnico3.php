<?php

/**
 * This is the model class for table "getReporteTecnico".
 *
 * The followings are the available columns in table 'getReporteTecnico':
 * @property integer $orden_id
 * @property string $estado
 * @property string $username
 * @property integer $iduser
 * @property string $fecha
 * @property string $fecha_desde
 * @property string $estado
 * @property string $fecha_hasta
 * @property string $porcen
 * @property integer $id
 * @property double $totalRep
 * @property double $importe
 */
class GetReporteTecnico extends CActiveRecord
{

    public $fecha_desde;
    public $fecha_hasta;
    public $estado;
    public $porcen;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return GetReporteTecnico the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function primaryKey(){
        return 'orden_id';
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'getReporteTecnico';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('estado, fecha', 'required'),
            array('orden_id, iduser, id', 'numerical', 'integerOnly'=>true),
            array('totalRep, importe', 'numerical'),
            array('estado', 'length', 'max'=>50),
            array('username', 'length', 'max'=>64),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('orden_id, estado, username, iduser, fecha, id, totalRep, importe, aPagar, porcentaje', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    public function getTotals($records)
    {
        $total = 0;
        foreach ($records as $record) {
                $total += $record->aPagar;
        }
        return "Total ARS: ".$total;


        /*if (!empty($ids)) {
            $ids = implode(",",$ids);
            $connection=Yii::app()->db;
            $command=$connection->createCommand("SELECT SUM(importe) FROM `orden_importe` where oid in ($ids)");
            return "Total ARS: ".$amount = $command->queryScalar();
        }else{
            return "Total ARS: 0";
        }*/
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'orden_id' => 'Orden',
            'estado' => 'Estado',
            'username' => 'Username',
            'iduser' => 'Iduser',
            'fecha' => 'Fecha',
            'id' => 'ID',
            'totalRep' => 'Importe Repuesto',
            'importe' => 'Importe',
            'aPagar' => 'Comision',
            'porcentaje' => 'Porcentaje'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('orden_id',$this->orden_id);
        $criteria->compare('username',$this->username,true);
        $criteria->compare('iduser',$this->iduser);
        $criteria->compare('fecha',$this->fecha,true);
        $criteria->compare('id',$this->id);
        $criteria->compare('totalRep',$this->totalRep);
        $criteria->compare('importe',$this->importe);
        $criteria->compare('aPagar',$this->importe);
        $criteria->compare('porcentaje',$this->importe);

        if(($this->estado != '') && ($this->estado != '')) {
            $criteria->condition = "estado = '{$this->estado}'";
        }

        if ($criteria->condition != "") {
            $criteria->condition = $criteria->condition . " and ";
        }

        if(($this->fecha_desde != '') && ($this->fecha_hasta != '')) {
            $criteria->condition = $criteria->condition . "fecha BETWEEN '{$this->fecha_desde}' AND '{$this->fecha_hasta}'";
        }

        if ($criteria->condition != "") {
            $criteria->condition = $criteria->condition . " and ";
        }

        if ($this->iduser != '') {
            $criteria->condition = $criteria->condition . "iduser = '{$this->iduser}'";
        }

        if ($this->porcen != '' and is_numeric($this->porcen)) {
            $criteria->select = 'orden_id, estado, username, iduser, fecha, id, estado_id, totalRep, importe, IF(ISNULL(totalRep), ROUND((importe/100)*'.$this->porcen.',2), ROUND(((importe-totalRep)/100)*'.$this->porcen.',2)) AS aPagar, '.$this->porcen.' AS porcentaje';
        }

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'sort' => array('defaultOrder' => 'id desc'),
            'pagination'=>array('pageSize'=>50),
        ));
    }
}