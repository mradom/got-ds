<?php

/**
 * This is the model class for table "tecnico_orden".
 *
 * The followings are the available columns in table 'tecnico_orden':
 * @property integer $toid
 * @property integer $tid
 * @property integer $oid
 * @property datetime $fecha_terminado
 */
class TecnicoOrden extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return TecnicoOrden the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'tecnico_orden';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('tid, oid', 'required'),
            array('tid, oid', 'numerical', 'integerOnly'=>true),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('toid, tid, oid, fecha_terminado', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'tecnicos' => array(self::HAS_MANY, 'GetTecnicos', 'iduser'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'toid' => 'Toid',
            'tid' => 'Tid',
            'oid' => 'Oid',
            'fecha_terminado' => 'Fecha Terminado'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('toid',$this->toid);
        $criteria->compare('tid',$this->tid);
        $criteria->compare('oid',$this->oid);
        $criteria->compare('fecha_terminado',$this->fecha_terminado);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }
}