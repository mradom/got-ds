<?php

/**
 * This is the model class for table "orden_importe".
 *
 * The followings are the available columns in table 'orden_importe':
 * @property integer $oid
 * @property double $importe
 *
 * The followings are the available model relations:
 * @property Orden $o
 */
class OrdenImporte extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return OrdenImporte the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'orden_importe';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('oid, importe', 'required'),
            array('oid', 'numerical', 'integerOnly'=>true),
            array('importe', 'numerical'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('oid, importe', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'o' => array(self::BELONGS_TO, 'Orden', 'oid'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'oid' => 'Oid',
            'importe' => 'Importe',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('oid',$this->oid);
        $criteria->compare('importe',$this->importe);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }
}