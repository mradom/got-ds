<?php

/**
 * This is the model class for table "getReporteTecnico".
 *
 * The followings are the available columns in table 'getReporteTecnico':
 * @property integer $id
 * @property string $aparato
 * @property string $fecha_terminada
 * @property string $username
 * @property integer $iduser
 * @property double $importe
 * @property integer $porcentaje
 * @property double $aPagar
 */
class GetReporteTecnico extends CActiveRecord
{

    public $fecha_desde;
    public $fecha_hasta;

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return GetReporteTecnico the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'getReporteTecnico';
	}

	public function primaryKey(){
        return 'id';
    }

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('aparato, importe', 'required'),
			array('id, iduser, porcentaje', 'numerical', 'integerOnly'=>true),
			array('importe, aPagar', 'numerical'),
			array('aparato', 'length', 'max'=>50),
			array('username', 'length', 'max'=>64),
			array('fecha_terminada', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, aparato, fecha_terminada, username, iduser, importe, porcentaje, aPagar', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'aparato' => 'Aparato',
			'fecha_terminada' => 'Fecha Terminada',
			'username' => 'Username',
			'iduser' => 'Iduser',
			'importe' => 'Importe',
			'porcentaje' => 'Porcentaje',
			'aPagar' => 'A Pagar',
		);
	}

    public function getTotals($records)
    {
        $total = 0;
        foreach ($records as $record) {
                $total += $record->aPagar;
        }
        return "Total ARS: ".$total;
    }

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('aparato',$this->aparato,true);
		$criteria->compare('fecha_terminada',$this->fecha_terminada,true);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('iduser',$this->iduser);
		$criteria->compare('importe',$this->importe);
		$criteria->compare('porcentaje',$this->porcentaje);
		$criteria->compare('aPagar',$this->aPagar);

		/*echo "<pre>";
		print_r($criteria->condition);
		echo "</pre>";
		die();*/

        if(($this->iduser != '') && ($this->fecha_desde != '') && ($this->fecha_hasta != '')) {
            $criteria->condition = $criteria->condition . " and fecha_terminada BETWEEN '{$this->fecha_desde}' AND '{$this->fecha_hasta}'";
        }

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'sort' => array('defaultOrder' => 'id desc'),
            'pagination'=>array('pageSize'=>50),
        ));
	}
}