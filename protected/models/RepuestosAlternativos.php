
<?php

/**
 * This is the model class for table "repuestos2".
 *
 * The followings are the available columns in table 'repuestos2':
 * @property integer $id
 * @property string $marca
 * @property string $modelo
 * @property string $codigo
 * @property string $detalle
 * @property double $precio
 * @property integer $cantidad
 */
class RepuestosAlternativos extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return RepuestosAlternativos the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'repuestos2';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('cantidad', 'numerical', 'integerOnly'=>true),
            array('precio', 'numerical'),
            array('marca, modelo, codigo', 'length', 'max'=>50),
            array('detalle', 'length', 'max'=>100),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, marca, modelo, codigo, detalle, precio, cantidad', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'marca' => 'Marca',
            'modelo' => 'Modelo',
            'codigo' => 'Codigo',
            'detalle' => 'Detalle',
            'precio' => 'Precio',
            'cantidad' => 'Cantidad',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('marca',$this->marca,true);
        $criteria->compare('modelo',$this->modelo,true);
        $criteria->compare('codigo',$this->codigo,true);
        $criteria->compare('detalle',$this->detalle,true);
        $criteria->compare('precio',$this->precio);
        $criteria->compare('cantidad',$this->cantidad);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }
}