<?php

class HistorialController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		/*return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);*/
                /* USANDO CRUGE */
                return array(array('CrugeAccessControlFilter'));
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{

		$model=new Historial;

		$this->performAjaxValidation($model);

		$orden = Orden::model()->findByPk($_POST['Historial']['orden_id']);
		$cliente = Cliente::model()->findByPk($orden->cli_id);
		$tecnicoOrden = TecnicoOrden::model()->findByAttributes(array('oid'=>$orden->id));

		if(isset($_POST['Historial']))
		{

			if ($_POST["Historial"]["estado_id"] == 1) {
				unset($_POST['Historial']['fecha_entrega']);
			}

			$_POST['Historial']['fecha'] = new CDbExpression('NOW()');
			$model->attributes=$_POST['Historial'];

			if (isset($_POST["Historial"]["importe"]) and $_POST['Historial']['importe'] != null) {

		        $OrdenImporte = OrdenImporte::model()->find('oid=:oid', array(':oid'=>$_POST['Historial']['orden_id']));
		        if ($OrdenImporte != null) {
		        	$OrdenImporte->delete();
		        }

				$OrdenImporte = new OrdenImporte();
				$OrdenImporte->oid = $_POST['Historial']['orden_id'];
				$OrdenImporte->importe = $_POST['Historial']['importe'];
				$OrdenImporte->save();
			}

			if($model->save()){
				$orden->estado_actual = $_POST["Historial"]["estado_id"];
				if ($_POST["Historial"]["estado_id"] == 2) {

					$orden->importe = $_POST['Historial']['importe'];

					Yii::app()->crugemailer->solicitaAprobacion($cliente, "Solicitamos Aprobacion de Orden de Trabajo", $this->getBody($cliente));
				}

				// Cuando se aprueba la orden se guarda la fecha de entrega
				if ($_POST["Historial"]["estado_id"] == 3)
				{
					$orden->fecha_pactada = $_POST['Historial']['fecha_entrega'];
				}

				if ($_POST["Historial"]["estado_id"] == 5) {
					// Guardo la fecha en la que fue terminada
					$orden->fecha_terminada = $_POST['Historial']['fecha'];
					$tecnicoOrden->fecha_terminado = $orden->fecha_terminada;
					$tecnicoOrden->save();
					$mensaje = "Sr. Cliente la orden ".$_POST["Historial"]["orden_id"]." ha finalizado y esta lista para retirar. DigitalService";
					Yii::app()->crugemailer->solicitaAprobacion($cliente, "Finalizacion de Orden de Trabajo", $mensaje);
				}

				if ($_POST['Historial']['estado_id'] == 6 || $_POST['Historial']['estado_id'] == 8) {
					$orden->fecha_entregada = $_POST['Historial']['fecha'];
				}

				$orden->update();

				$this->redirect(array('/orden/'.$model->orden_id));
			}else {
				echo "<pre>";
				print_r($this);
				print_r($model->getErrors());
				echo "</pre>";
				die("HISTORIALCONTROLADOR.php CREATE");
			}
		}
	}

	private function getBody(Cliente $cliente){
		return "<html>
<head>
    <title></title>
</head>
<body>
<div style='background-image: url(http://www.digitalservicecba.com.ar/sites/all/themes/digital_service/images/bg_header.jpg);background-repeat: no-repeat;'>
    <img style='margin-left:235px;' src='http://www.digitalservicecba.com.ar/sites/all/themes/digital_service/images/logo.png'>
</div>
<div style='width:637px;'>
<h2>Presupuesto de Servicio Tecnico</h2>
Estimado cliente:<br>
La orden ".$_POST["Historial"]["orden_id"]." espera su aprobacion.<br>
<b>Tarea:</b> ".$_POST['Historial']['observacion']."<br>
<b>Importe:</b> $ ".$_POST["Historial"]["importe"]."</br><br>
Usted puede aprobar el siguiente trabajo haciendo <a href='http://clientes.digitalservicecba.com.ar/'>click aca</a>!

O bien copiar el siguiente link y pegarlo en el navegador.
<br><br>
http://clientes.digitalservicecba.com.ar/
<br>Los datos de acceso son:
<br>Usuario: ".$cliente->dni."
<br>Clave: ".$cliente->clave."
<br><br>
<h4>Como aprobar una orden?</h4>
    <ul>
        <li>Ingresar a nuestra <a href='http://clientes.digitalservicecba.com.ar/'>Intranet</a>.</li>
        <li>En el campo de usuario ingresar su DNI.</li>
        <li>En el campo de clave ingresar la clave indicada anteriormente.</li>
        <li>El sistema mostrara un listado de ordenes.</li>
        <li>Las ordenes pendiente de aprobación, mostrara un boton `Aprobar`.</li>
        <li>Click en aprobar y mostrara informacion sobre presupuesto y fecha estimada de entrega.</li>
        <li>Al hacer click en `Aceptar Presupuesto` usted estara aprobando el presupuesto.</li>
        <li>Nuestro equipo de tenicos trabajara en su orden.</li>
    </ul>
</div>
<div style='background-color:#343434;width:637px;'>
<img src='http://www.digitalservicecba.com.ar/sites/all/themes/digital_service/images/logo_footer.jpg'>
<div style='float:right; margin:20px;'>
    <font color='white' >
        Belgrano 94 - Cordoba<br>
        Tel: (0351) 4237572<br>
        Whatsapp: 3516102103<br>
        <a href='http://www.digitalservicecba.com.ar/' target='_BLANK'>www.digitalservicecba.com.ar</a>
    </font>
</div>
</div>
</body>
</html>";
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Historial']))
		{
			$model->attributes=$_POST['Historial'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Historial');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Historial('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Historial']))
			$model->attributes=$_GET['Historial'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Historial the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Historial::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Historial $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='historial-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	public function guardarTecnico(TecnicoOrden $tecnicoOrden, $estadoActual){
		$historial = new Historial();
		$historial->orden_id = $tecnicoOrden->oid;
		$historial->estado_id = $estadoActual;
		$historial->fecha = new CDbExpression('NOW()');
		$historial->observacion = "Se asigna tecnico a esta orden";
		$historial->save();
		return true;
	}
}
